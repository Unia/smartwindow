#include <Stepper.h>
#include <MsTimer2.h>
#include <ESP8266_Lib.h>
#include <BlynkSimpleShieldEsp8266.h>

#define BLYNK_PRINT Serial
#define DUST_MEASURE_PIN 1
#define END_CHECK_SW 3
#define MOTER_GND_SW 8
#define DUST_LED_POWER_PIN 12

#define EspSerial Serial
#define ESP8266_BAUD 9600
ESP8266 wifi(&EspSerial);
char ssid[] = "miro";
char pass[] = "miro8025";

float dustVoMeasured = 0;
float dustCalcVoltage = 0;
float dustDensity = 0;

const int windowOpen = -100;
const int windowClose = 100;
const int windowNeutral = 0;

int stepsPerRevolution = windowNeutral; //360도
Stepper myStepper(200,7,6,5,4);
 // // // //웹 파싱 코딩 할것 그리고 전체적 실험 후 마무리

int checkState = 0; //OPEN = 1; CLOSE = -1; neutrality = 0;
byte checkTectSw = 0;
bool checkWindowClose = 0;
bool checkDustOne = 0;
bool checkRainOne = 0;
bool checkRunMoter = 0;

long lastTime = 0;

void setup()
{
  BlynkInit();
  analogWrite(5,1024);
  analogWrite(4,1024);
  pinMode(MOTER_GND_SW, OUTPUT);
  digitalWrite(MOTER_GND_SW, LOW);
  pinMode(DUST_LED_POWER_PIN,OUTPUT);
  pinMode(END_CHECK_SW, INPUT);
  myStepper.setSpeed(65);
  Serial.begin(9600);

 // attachInterrupt(digitalPinToInterrupt(END_CHECK_SW), StopMoter, RISING);
  MsTimer2::set(1000,peripheryScan);
  MsTimer2::start();
  moterOFF();
}

void loop()
{
  Blynk.run();
//  for(int k = 0 ; k < 10 ; k++)
//  {
//    peripheryScan();
  StartMoter();
}

void StartMoter()//OPEN = 1; CLOSE = -1; neutrality = 0;
{
    if(!digitalRead(END_CHECK_SW))
    {
      if(checkState == 0)
      {
        moterOFF();
      }
      else if(checkState == 1)
      {
        MsTimer2::stop();
        myStepper.step(windowOpen);
        Serial.println(digitalRead(END_CHECK_SW));
        if(digitalRead(END_CHECK_SW))
        {
          MsTimer2::start();
          myStepper.step(windowNeutral);
          delay(500);
          checkState = 0;
          moterOFF();
        }
      }
      else if(checkState == -1)
      {
        MsTimer2::stop();
        myStepper.step(windowClose);
        if(digitalRead(END_CHECK_SW))
        {
          MsTimer2::start();
          myStepper.step(windowNeutral);
          checkState = 0;
          moterOFF();
         }
      }
    }
}

void moterON()
{
  pinMode(7,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(4,OUTPUT);
}

void moterOFF()
{
  pinMode(7,INPUT);
  pinMode(6,INPUT);   
  pinMode(5,INPUT);
  pinMode(4,INPUT);  
}

void BlynkInit()
{
  char auth[] = "bcba1dff0892437c9eac1616d758be9f";
  EspSerial.begin(ESP8266_BAUD);
  delay(10);
  Blynk.begin(auth, wifi, ssid, pass);
}

BLYNK_WRITE(0) 
{
  if(param.asInt())
  {
    moterON();
    checkState = 1;
  }
  else
  {
    moterOFF();
    checkState = 0;
  }
}
BLYNK_WRITE(1)
{
  if(param.asInt())
  {
    moterON();
    checkState = -1;
  }
  else
  {
    moterOFF();
    checkState = 0;
  }
}

int averageRain[8] = {1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024};
int averageRainAddress = 0;
int sumRain = 8192;

int GetAverageRain()
{
  if(averageRainAddress == 8)
  {
    averageRainAddress = 0;
  }
  sumRain -= averageRain[averageRainAddress];
  averageRain[averageRainAddress] = analogRead(0);
  sumRain += averageRain[averageRainAddress];
  averageRainAddress++;
  return (sumRain / 8);  
}

int averageDust[3] = {0, };
int averageDustAddress = 0;
int sumDust = 0;

int GetAverageDust()
{
  if(averageDustAddress == 3)
  {
    averageDustAddress = 0;
  }
  sumDust -= averageDust[averageDustAddress];
  averageDust[averageDustAddress] = Getdust();
  sumDust += averageDust[averageDustAddress];
  averageDustAddress++;
  return (sumDust / 3);
}

int Getdust()
{
  const int dustSamplingTime = 280;
  const int dustDeltaTime = 40;
  const int dustSleepTime = 9680;
  digitalWrite(DUST_LED_POWER_PIN,LOW);
  delayMicroseconds(dustSamplingTime);
  dustVoMeasured = analogRead(DUST_MEASURE_PIN);
  delayMicroseconds(dustDeltaTime);
  digitalWrite(DUST_LED_POWER_PIN,HIGH);
  delayMicroseconds(dustSleepTime);
  dustCalcVoltage = dustVoMeasured * (3.3 / 1024);
  dustDensity = 100 * dustCalcVoltage - 0.1;
  return map(dustDensity,8,133,10,100);
}

int webParsingDust = 0; //xml 파싱후 결과값 저장 용
const int maxDust = 85;
const int minDust = 40;

void peripheryScan()
{
  int rain = GetAverageRain();
  int dust = GetAverageDust();
  if(rain < 400)
  {
    checkRainOne = true;
  }
  else
  {
    checkRainOne = false;
  }
  if(dust > maxDust  & dust > webParsingDust & !checkDustOne) // 밖 미세먼지가 적음 - 문 열음
  {
    checkDustOne = true;
  }
  else if(minDust > dust & checkDustOne)
  {
    checkDustOne = false;
  }

  
  Serial.print("checkRainOne : ");
  Serial.print(checkRainOne);
  Serial.print(" checkDustOne : ");
  Serial.print(checkDustOne);
  Serial.print(" checkRunMoter : ");
  Serial.print(checkRunMoter);  
  if(!checkRunMoter & !checkRainOne & checkDustOne)
  {
    moterON();
    checkState = 1;
  }
  else if(!checkRunMoter & !checkRainOne & !checkDustOne)
  {
    checkState = 0;
  }
  else if(!checkRunMoter)
  {
    moterON();
    checkState = -1;
  }
  Serial.print(" data : ");
  Serial.print(rain);
  Serial.print(" AverageDust : ");
  Serial.print(dust);
  Serial.print(" checkState : ");
  Serial.print(checkState);
  Serial.println("");
/*  
  if(rain < 400 & !checkRainOne)
  {
    moterON();
    checkState = -1;
    checkRainOne = 1;
  }
  else
  {
    if(webParsingDust > dust & dust > maxDust & !checkDustOne) //밖 미세먼지가 많음 - 문 닫음
    {
      moterON();
      checkState = -1;
      checkDustOne = 1;
    }
    else if(dust > maxDust  & dust > webParsingDust & !checkDustOne) // 밖 미세먼지가 적음 - 문 열음
    {
      moterON();
      checkState = 1;
      checkDustOne = 1;
    }
    else if(minDust > dust  & checkDustOne)
    {
      checkDustOne = 0;
    }
  }*/
/*  Serial.print("checkDustOne : ");
  Serial.print(checkDustOne);
  Serial.println();*/  
//  Serial.print("stepsPerRevolution : ");
//  Serial.print(stepsPerRevolution);
//  Serial.println();
//  delay(1000);
}

