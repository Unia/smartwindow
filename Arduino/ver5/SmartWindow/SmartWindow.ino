﻿/*
 Name:		SmartWindow.ino
 Created:	2018-04-07 오후 3:13:42
 Author:	kphk4
 ETC : StepMoter Drive full step
 */
#define CAYENNE_DEBUG
#define CAYENNE_PRINT Serial

#define VIRTUAL_CHANNEL 2

#define SENSOR_PIN 5
#define ACTUATOR_PIN 4

#define DUST_READ_PIN 10
#define DUST_LED_PIN 11
#define DUST_AVERAGE_COUNT 3

#define RAIN_DIGITAL_PIN 10
#define RAIN_ANALOGIN_PIN 11
#define RAIN_AVERAGE_COUNT 8

#define MOTOR_STEP 2
#define MOTOR_DIP 3
#define MOTOR_ENABLE 11

#define MOTOR_START 0
#define MOTOR_STOP 1

#define LEFT_CONTRL_PIN 1
#define RIGHT_CONTRL_PIN 4

#define LEFT_CHECK_PIN 5
#define RIGHT_CHECK_PIN 6

#define RIGHT_MOVE_DIP 0
#define LEFT_MOVE_DIP 1

#define MOTOR_SPEED 700 //2.8mS

#define RAIN_OVER 200
#define DUST_OVER 200

#include <WiFi101.h>
#include <CayenneMQTTMKR1000.h>
#include <Timer5.h>

bool mState = RIGHT_MOVE_DIP;
bool mPulseBool = HIGH;

const char ssid[] = "iptimeHome";
const char wifiPassword[] = "kp20772077";

const char username[] = "ee838270-323d-11e8-9eaa-434cc97e08c2";
const char password[] = "c8184b223a6d7b02e9581f044d722a5682897967";
const char clientID[] = "09f36bb0-4071-11e8-9eaa-434cc97e08c2";

void setup()
{
	Serial.begin(9600);
	pinMode(DUST_READ_PIN, INPUT);
	pinMode(DUST_LED_PIN, OUTPUT);
	pinMode(MOTOR_STEP, OUTPUT);
	pinMode(MOTOR_DIP, OUTPUT);
	pinMode(MOTOR_ENABLE, OUTPUT);

	pinMode(LEFT_CONTRL_PIN, INPUT);
	pinMode(RIGHT_CONTRL_PIN, INPUT);
	pinMode(LEFT_CHECK_PIN, INPUT);
	pinMode(RIGHT_CHECK_PIN, INPUT);

	pinMode(12, OUTPUT);

	attachInterrupt(digitalPinToInterrupt(RIGHT_CONTRL_PIN), IRightMove, FALLING);
	attachInterrupt(digitalPinToInterrupt(LEFT_CONTRL_PIN), ILeftMove, FALLING);
	attachInterrupt(digitalPinToInterrupt(LEFT_CHECK_PIN), IRightEndCheck, FALLING);
	attachInterrupt(digitalPinToInterrupt(RIGHT_CHECK_PIN), ILeftEndCheck, FALLING);

	MotorStart();
	MyTimer5.begin(MOTOR_SPEED);
	MyTimer5.attachInterrupt(MotorPulse);
	MyTimer5.start();

	Cayenne.begin(username, password, clientID, ssid, wifiPassword);
}

void loop()
{
	Cayenne.loop();
	if (GetAvgRain() > RAIN_OVER || GetAvgDust() < DUST_OVER) //outside 
	{
		WindowClose();
	}
	else
	{
		WindowOpen();
	}
}

int mAverageDust[3] = { 0, };
int mAverageDustAddress = 0;
int mSumDust = 0;

int GetAvgDust()
{
	if (mAverageDustAddress == DUST_AVERAGE_COUNT)
	{
		mAverageDustAddress = 0;
	}
	mSumDust -= mAverageDust[mAverageDustAddress];
	mAverageDust[mAverageDustAddress] = ReadDustSensor();
	mSumDust += mAverageDust[mAverageDustAddress];
	mAverageDustAddress++;
	return mSumDust / DUST_AVERAGE_COUNT;
}

int ReadDustSensor()
{
	digitalWrite(DUST_LED_PIN, LOW);
	delayMicroseconds(280);
	int dustVal = analogRead(DUST_READ_PIN);
	delayMicroseconds(40);
	digitalWrite(DUST_LED_PIN, HIGH);
	delayMicroseconds(9680);
	return map(dustVal, 8, 133, 10, 100);
}

int mAverageRain[8] = { 1024, };
int mAverageRainAddress = 0;
int mSumRain = 8192;

int GetAvgRain()
{
	if (mAverageRainAddress == RAIN_AVERAGE_COUNT)
	{
		mAverageRainAddress = 0;
	}
	mSumRain -= mAverageRain[mAverageRainAddress];
	mAverageRain[mAverageRainAddress] = analogRead(0);
	mSumRain += mAverageRain[mAverageRainAddress];
	mAverageRainAddress++;
	return mSumRain / RAIN_AVERAGE_COUNT;
}
/*test 할것 */
int GetAvg(int *sum, int *array, int size, int *Count)
{
	if (*Count == size)
	{
		*Count = 0;
	}
	*sum -= *(array + *Count);
	*(array + *Count) = analogRead(0);
	*sum += *(array + *Count);
	*Count++;
	return *sum / size;
}

void IRightMove()
{
	MotorStop();
	mState = RIGHT_MOVE_DIP;
	digitalWrite(MOTOR_DIP, mState);
	MotorStart();
}

void ILeftMove()
{
	MotorStop();
	mState = LEFT_MOVE_DIP;
	digitalWrite(MOTOR_DIP, mState);
	MotorStart();
}

bool rightEnd = false;
void IRightEndCheck()
{
	rightEnd = true;
	MotorStop();
}

void ILeftEndCheck()
{
	MotorStop();
}

void WindowOpen()
{
	bool leftNotEnd = digitalRead(LEFT_CHECK_PIN);
	if (leftNotEnd == true)
	{
		ILeftMove();
	}
}

void WindowClose()
{
	bool leftNotEnd = digitalRead(LEFT_CHECK_PIN);
	if (leftNotEnd == true)
	{
		ILeftMove();
		detachInterrupt(LEFT_CHECK_PIN);
		while (true)
		{
			if (digitalRead(LEFT_CHECK_PIN) == true)
			{
				IRightMove();
				break;
			}
		}
		attachInterrupt(digitalPinToInterrupt(LEFT_CHECK_PIN), IRightEndCheck, FALLING);
	}
	else
	{
		IRightMove();
	}
}

void MotorPulse()
{
	digitalWrite(MOTOR_STEP, mPulseBool = !mPulseBool);
}

void MotorStop()
{
	digitalWrite(MOTOR_ENABLE, true);
}

void MotorStart()
{
	digitalWrite(MOTOR_ENABLE, false);
}

CAYENNE_OUT_DEFAULT()
{
	// Write data to Cayenne here. This example just sends the current uptime in milliseconds on virtual channel 0.
	Cayenne.virtualWrite(0, millis());
	// Some examples of other functions you can use to send data.
	//Cayenne.celsiusWrite(1, 22.0);
	//Cayenne.luxWrite(2, 700);
	//Cayenne.virtualWrite(3, 50, TYPE_PROXIMITY, UNIT_CENTIMETER);
}

// Default function for processing actuator commands from the Cayenne Dashboard.
// You can also use functions for specific channels, e.g CAYENNE_IN(1) for channel 1 commands.
CAYENNE_IN_DEFAULT()
{
	CAYENNE_LOG("Channel %u, value %s", request.channel, getValue.asString());
	//Process message here. If there is an error set an error message using getValue.setError(), e.g getValue.setError("Error message");
}

#define DUST_CHANNEL 2
CAYENNE_OUT(DUST_CHANNEL)
{
	Cayenne.virtualWrite(DUST_CHANNEL, GetAvgDust());
}

#define RAIN_CHANNEL 3
CAYENNE_OUT(RAIN_CHANNEL)
{
	Cayenne.virtualWrite(RAIN_CHANNEL, GetAvgRain());
}

#define MOTOR_RIGHT_CHANNEL 4
CAYENNE_IN(VIRTUAL_CHANNEL)
{
	bool rightNotEnd = digitalRead(RIGHT_CHECK_PIN);
	bool enabled = getValue.asInt();
	if (rightNotEnd == true & enabled == true)
	{
		IRightMove();
	}
}

#define MOTOR_LEFT_CHANNEL 5
CAYENNE_IN(VIRTUAL_CHANNEL)
{
	bool leftNotEnd = digitalRead(LEFT_CHECK_PIN);
	bool enabled = getValue.asInt();
	if (leftNotEnd == true && enabled == true)
	{
		ILeftMove();
	}
}

#define MOTOR_STOP_CHANNEL 6
CAYENNE_IN(VIRTUAL_CHANNEL)
{
	MotorStop();
}