#include <Stepper.h>
#include <MsTimer2.h>
#include <ESP8266_Lib.h>
#include <BlynkSimpleShieldEsp8266.h>

#define BLYNK_PRINT Serial
#define DUST_MEASURE_PIN 1
#define END_CHECK_SW 3
#define MOTER_GND_SW 8
#define DUST_LED_POWER_PIN 12

#define EspSerial Serial
#define ESP8266_BAUD 9600
ESP8266 wifi(&EspSerial);
char ssid[] = "iptime426";
char pass[] = "20772077";

float dustVoMeasured = 0;
float dustCalcVoltage = 0;
float dustDensity = 0;

const int windowOpen = -200;
const int windowClose = 200;
const int windowNeutral = 0;

int stepsPerRevolution = windowNeutral; //360도
Stepper myStepper(200,7,6,5,4);
 // // // //웹 파싱 코딩 할것 그리고 전체적 실험 후 마무리

int checkState = 0; //OPEN = 1; CLOSE = -1; neutrality = 0;
byte checkTectSw = 0;
bool checkWindowClose = 0;
bool checkDustOne = 0;
bool checkRainOne = 0;

void setup()
{
//  BlynkInit();
  analogWrite(5,1024);
  analogWrite(4,1024);
  pinMode(MOTER_GND_SW, OUTPUT);
  digitalWrite(MOTER_GND_SW, LOW);
  pinMode(DUST_LED_POWER_PIN,OUTPUT);
  pinMode(END_CHECK_SW, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(END_CHECK_SW), StopMoter, RISING);
  myStepper.setSpeed(80);
  Serial.begin(9600);
  MsTimer2::set(500,peripheryScan);
  MsTimer2::start();
  moterOFF();
}

void loop()
{
//  Blynk.run();
//  for(int k = 0 ; k < 10 ; k++)
//  {
    StartMoter();
//  }
}

void StartMoter()//OPEN = 1; CLOSE = -1; neutrality = 0;
{
  if(checkState == 1 & checkTectSw == 0)
  {
    stepsPerRevolution = windowOpen;
//    Serial.print("[A] ");
  }
  else if(checkState == 1 & checkTectSw == 255)
  {
    checkState = 0;
//    Serial.print("[B] ");
  }
  else if(checkState == -1 & checkTectSw == 0 & !checkWindowClose)
  {
    stepsPerRevolution = windowOpen;
//    Serial.print("[C] ");
  }
  else if(checkState == -1 & checkTectSw == 255 & !checkWindowClose)
  {
    moterON();
    checkWindowClose = 1;
    delay(10);
//    Serial.print("[D] ");
  }
  else if(checkState == -1 & checkTectSw == 255 & checkWindowClose)
  {
    stepsPerRevolution = windowClose;
//    Serial.print("[E] ");
  }
  else if(checkState == -1  & checkTectSw == 0 & checkWindowClose)
  {
    checkTectSw = 0;
    checkState = 0;
    checkWindowClose = 0;
    checkRainOne = 0;
    moterOFF();
//    Serial.print("[F] ");
  }
  Serial.print("checkState : ");
  Serial.print(checkState);
  Serial.print(" checkTectSw : ");
  Serial.print(checkTectSw);
  Serial.print(" checkWindowClose : ");
  Serial.print(checkWindowClose);
  Serial.println();
  myStepper.step(stepsPerRevolution);
}

void moterON()
{
  pinMode(7,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(4,OUTPUT);
}

void moterOFF()
{
  pinMode(7,INPUT);
  pinMode(6,INPUT);   
  pinMode(5,INPUT);
  pinMode(4,INPUT);  
}

void StopMoter()
{
  checkTectSw = ~checkTectSw;
/*  if(checkTectSw)
  {
    checkTectSw = 0;
  }
  else
  {
    checkTectSw = 1;
  }*/
  moterOFF();
  stepsPerRevolution = windowNeutral;
}

void BlynkInit()
{
  char auth[] = "5764db8c390d4792b6f385166242c95b";
  EspSerial.begin(ESP8266_BAUD);
  delay(10);
  Blynk.begin(auth, wifi, ssid, pass);
}

BLYNK_WRITE(0) 
{
  if(param.asInt())
  {
    moterON();
    stepsPerRevolution = windowOpen;
  }
  else
  {
    moterOFF();
    stepsPerRevolution = windowNeutral;
  }
}
BLYNK_WRITE(1)
{
  if(param.asInt())
  {
    moterON();
    stepsPerRevolution = windowClose;
  }
  else
  {
    moterOFF();
    stepsPerRevolution = windowNeutral;
  }
}

int averageRain[8] = {1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024};
int averageRainAddress = 0;
int sumRain = 8192;

int GetAverageRain()
{
  if(averageRainAddress == 8)
  {
    averageRainAddress = 0;
  }
  sumRain -= averageRain[averageRainAddress];
  averageRain[averageRainAddress] = analogRead(0);
  sumRain += averageRain[averageRainAddress];
  averageRainAddress++;
  return (sumRain / 8);  
}

int averageDust[3] = {0, };
int averageDustAddress = 0;
int sumDust = 0;

int GetAverageDust()
{
  if(averageDustAddress == 3)
  {
    averageDustAddress = 0;
  }
  sumDust -= averageDust[averageDustAddress];
  averageDust[averageDustAddress] = Getdust();
  sumDust += averageDust[averageDustAddress];
  averageDustAddress++;
  return (sumDust / 3);
}

int Getdust()
{
  const int dustSamplingTime = 280;
  const int dustDeltaTime = 40;
  const int dustSleepTime = 9680;
  digitalWrite(DUST_LED_POWER_PIN,LOW);
  delayMicroseconds(dustSamplingTime);
  dustVoMeasured = analogRead(DUST_MEASURE_PIN);
  delayMicroseconds(dustDeltaTime);
  digitalWrite(DUST_LED_POWER_PIN,HIGH);
  delayMicroseconds(dustSleepTime);
  dustCalcVoltage = dustVoMeasured * (3.3 / 1024);
  dustDensity = 100 * dustCalcVoltage - 0.1;
  return map(dustDensity,8,133,10,100);
}

int webParsingDust = 0; //xml 파싱후 결과값 저장 용
const int maxDust = 85;
const int minDust = 40;

void peripheryScan()
{
  int rain = GetAverageRain();
  int dust = GetAverageDust();
/*  Serial.print("data : ");
  Serial.print(rain);
  Serial.println("");
  Serial.print("AverageDust : ");
  Serial.print(dust);
  Serial.println("");*/
  
  if(rain < 400 & !checkRainOne)
  {
    moterON();
    checkState = -1;
    checkRainOne = 1;
    checkTectSw = 0;
  }
  else
  {
    if(webParsingDust > dust & dust > maxDust & !checkDustOne) //밖 미세먼지가 많음 - 문 닫음
    {
      moterON();
      checkState = -1;
      checkDustOne = 1;
      checkTectSw = 0;
    }
    else if(dust > maxDust  & dust > webParsingDust & !checkDustOne) // 밖 미세먼지가 적음 - 문 열음
    {
      moterON();
      checkState = 1;
      checkDustOne = 1;
      checkTectSw = 0;
    }
    else if(minDust > dust  & checkDustOne)
    {
      checkDustOne = 0;
    }
  }
/*  Serial.print("checkDustOne : ");
  Serial.print(checkDustOne);
  Serial.println();*/  
//  Serial.print("stepsPerRevolution : ");
//  Serial.print(stepsPerRevolution);
//  Serial.println();
//  delay(1000);
}

